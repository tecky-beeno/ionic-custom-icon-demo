import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'io.ionic.starter',
  appName: 'ionic-custom-icon-demo',
  webDir: 'build',
  bundledWebRuntime: false
};

export default config;
